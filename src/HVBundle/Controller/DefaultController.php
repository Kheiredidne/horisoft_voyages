<?php

namespace HVBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
    /**
     * @Route("/index")
    */
    public function indexAction()
    {

        return $this->render('@HVBundle/Resources/views/default/index.html.twig');
    }
}
