<?php

namespace HVBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HTTPFoundation\Request;
use HVBundle\Entity\Count;
use HVBundle\Form\CountType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use HVBundle\Repository\Count\CountRepository;


class CountController extends Controller
{

    protected $countRepository;
    /**
     * @Route("/addCount",name="add_action")
     */
   public function CountAction()
   {
           $count=new Count();
           
           $form=$this->createform(new CountType(), $count);
           $request=$this->get('request');
           if($request->isMethod('POST'))
           {
               $form->bind($request);
               /*$connexion=$this->getDoctrine()->getManager();
               $connexion->persist($count);
               $connexion->flush();*/

               $this->get('countServiceId')->addCount($count);
           }
   return $this->render('HVBundle:Count:count.html.twig',array('form'=>$form->createView()));
   }

    /**
     * @Route("/showCount")
     */
   public function showAction()
   {       
       $count=new Count();
           
           $form=$this->createform(new CountType(), $count);
  
    return  $this->render('HVBundle:Count:count.html.twig',array('form'=>$form->createView()));
}}
?>
