<?php
/**
 * Created by PhpStorm.
 * User: khayri
 * Date: 20/03/2016
 * Time: 02:19
 */

namespace HVBundle\Service\Count;


use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpKernel\Log\LoggerInterface;
use  HVBundle\Repository\Count\CountRepository;
use HVBundle\Entity\Count;

class CountService
{
    protected $countRepository;
    protected $logger;

    public function __construct(CountRepository $countRepository,
                                LoggerInterface $logger)
    {
        $this->countRepository = $countRepository;
        $this->logger = $logger;
    }

    public function query()
    {
        $results = $this->countRepository->fetchLatest();
        $this->logger->info(sprintf('Someone fetched %d foos',
            count($results)));

        return $results;
    }


    public function addCount(Count $count)
    {
        $results = $this->countRepository->addCount($count);
        $this->logger->info("success");

        return $results;
    }

}