<?php

namespace HVBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CountType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('etat')
            ->add('civilite')
            ->add('nom')
            ->add('prenom')
            ->add('datenaissance')
            ->add('nationalite')
            ->add('adresse')
            ->add('codepostale')
            ->add('ville')
            ->add('pays')
            ->add('telfixe')
            ->add('telportable')
            ->add('nopassport')
            ->add('dateexpiration')
            ->add('paysemission')
            ->add('heuredebutdc')
            ->add('heurefindc')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'HVBundle\Entity\Count'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'hvbundle_count';
    }
}
