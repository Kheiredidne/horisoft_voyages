<?php

namespace HVBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="HVBundle\Repository\Count\CountRepository")
 * @ORM\Table(name="count")
 */
class Count
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var string
     */
    private $etat;

    /**
     * @var string
     */
    private $civilite;

    /**
     * @var string
     */
    private $nom;

    /**
     * @var string
     */
    private $prenom;

    /**
     * @var \DateTime
     */
    private $datenaissance;

    /**
     * @var string
     */
    private $nationalite;

    /**
     * @var string
     */
    private $adresse;

    /**
     * @var string
     */
    private $codepostale;

    /**
     * @var string
     */
    private $ville;

    /**
     * @var string
     */
    private $pays;

    /**
     * @var integer
     */
    private $telfixe;

    /**
     * @var integer
     */
    private $telportable;

    /**
     * @var string
     */
    private $nopassport;

    /**
     * @var \DateTime
     */
    private $dateexpiration;

    /**
     * @var string
     */
    private $paysemission;

    /**
     * @var \DateTime
     */
    private $heuredebutdc;

    /**
     * @var \DateTime
     */
    private $heurefindc;

    
    /**
     * Set etat
     *
     * @param string $etat
     * @return Compte
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return string 
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * Set civilite
     *
     * @param string $civilite
     * @return Compte
     */
    public function setCivilite($civilite)
    {
        $this->civilite = $civilite;

        return $this;
    }

    /**
     * Get civilite
     *
     * @return string 
     */
    public function getCivilite()
    {
        return $this->civilite;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Compte
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     * @return Compte
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string 
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set datenaissance
     *
     * @param \DateTime $datenaissance
     * @return Compte
     */
    public function setDatenaissance($datenaissance)
    {
        $this->datenaissance = $datenaissance;

        return $this;
    }

    /**
     * Get datenaissance
     *
     * @return \DateTime 
     */
    public function getDatenaissance()
    {
        return $this->datenaissance;
    }

    /**
     * Set nationalite
     *
     * @param string $nationalite
     * @return Compte
     */
    public function setNationalite($nationalite)
    {
        $this->nationalite = $nationalite;

        return $this;
    }

    /**
     * Get nationalite
     *
     * @return string 
     */
    public function getNationalite()
    {
        return $this->nationalite;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     * @return Compte
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string 
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set codepostale
     *
     * @param string $codepostale
     * @return Compte
     */
    public function setCodepostale($codepostale)
    {
        $this->codepostale = $codepostale;

        return $this;
    }

    /**
     * Get codepostale
     *
     * @return string 
     */
    public function getCodepostale()
    {
        return $this->codepostale;
    }

    /**
     * Set ville
     *
     * @param string $ville
     * @return Compte
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string 
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set pays
     *
     * @param string $pays
     * @return Compte
     */
    public function setPays($pays)
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * Get pays
     *
     * @return string 
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * Set telfixe
     *
     * @param integer $telfixe
     * @return Compte
     */
    public function setTelfixe($telfixe)
    {
        $this->telfixe = $telfixe;

        return $this;
    }

    /**
     * Get telfixe
     *
     * @return integer 
     */
    public function getTelfixe()
    {
        return $this->telfixe;
    }

    /**
     * Set telportable
     *
     * @param integer $telportable
     * @return Compte
     */
    public function setTelportable($telportable)
    {
        $this->telportable = $telportable;

        return $this;
    }

    /**
     * Get telportable
     *
     * @return integer 
     */
    public function getTelportable()
    {
        return $this->telportable;
    }

    /**
     * Set nopassport
     *
     * @param string $nopassport
     * @return Compte
     */
    public function setNopassport($nopassport)
    {
        $this->nopassport = $nopassport;

        return $this;
    }

    /**
     * Get nopassport
     *
     * @return string 
     */
    public function getNopassport()
    {
        return $this->nopassport;
    }

    /**
     * Set dateexpiration
     *
     * @param \DateTime $dateexpiration
     * @return Compte
     */
    public function setDateexpiration($dateexpiration)
    {
        $this->dateexpiration = $dateexpiration;

        return $this;
    }

    /**
     * Get dateexpiration
     *
     * @return \DateTime 
     */
    public function getDateexpiration()
    {
        return $this->dateexpiration;
    }

    /**
     * Set paysemission
     *
     * @param string $paysemission
     * @return Compte
     */
    public function setPaysemission($paysemission)
    {
        $this->paysemission = $paysemission;

        return $this;
    }

    /**
     * Get paysemission
     *
     * @return string 
     */
    public function getPaysemission()
    {
        return $this->paysemission;
    }

    /**
     * Set heuredebutdc
     *
     * @param \DateTime $heuredebutdc
     * @return Compte
     */
    public function setHeuredebutdc($heuredebutdc)
    {
        $this->heuredebutdc = $heuredebutdc;

        return $this;
    }

    /**
     * Get heuredebutdc
     *
     * @return \DateTime 
     */
    public function getHeuredebutdc()
    {
        return $this->heuredebutdc;
    }

    /**
     * Set heurefindc
     *
     * @param \DateTime $heurefindc
     * @return Compte
     */
    public function setHeurefindc($heurefindc)
    {
        $this->heurefindc = $heurefindc;

        return $this;
    }

    /**
     * Get heurefindc
     *
     * @return \DateTime 
     */
    public function getHeurefindc()
    {
        return $this->heurefindc;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
