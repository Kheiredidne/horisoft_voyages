<?php
/**
 * Created by PhpStorm.
 * User: khayri
 * Date: 20/03/2016
 * Time: 02:24
 */

namespace HVBundle\Repository\Count;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping;
use Doctrine\ORM\EntityManager;
use HVBundle\Repository\BaseRepository;
use HVBundle\Repository\Count\CountRepositoryInterface;
use HVBundle\Entity\Count;

class CountRepository extends BaseRepository implements CountRepositoryInterface
{

    protected $entityManager;

    public function __construct(EntityManager $entityManager)
   {

       $this->entityManager = $entityManager;
   }


    public function addCount(Count $count)
    {
        $this->entityManager->persist($count);
        $this->entityManager->flush();
        /*$this->_em->persist($count);
        $this->_em->flush();*/
        // create query builder and fetch items sorted by date descending
    }

    /**
     * Finds an object by its primary key / identifier.
     *
     * @param mixed $id The identifier.
     *
     * @return object The object.
     */
    public function find($id)
    {
        // TODO: Implement find() method.
    }

    /**
     * Finds all objects in the repository.
     *
     * @return array The objects.
     */
    public function findAll()
    {
        // TODO: Implement findAll() method.
    }

    /**
     * Finds objects by a set of criteria.
     *
     * Optionally sorting and limiting details can be passed. An implementation may throw
     * an UnexpectedValueException if certain values of the sorting or limiting details are
     * not supported.
     *
     * @param array $criteria
     * @param array|null $orderBy
     * @param int|null $limit
     * @param int|null $offset
     *
     * @return array The objects.
     *
     * @throws \UnexpectedValueException
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        // TODO: Implement findBy() method.
    }

    /**
     * Finds a single object by a set of criteria.
     *
     * @param array $criteria The criteria.
     *
     * @return object The object.
     */
    public function findOneBy(array $criteria)
    {
        // TODO: Implement findOneBy() method.
    }

    /**
     * Returns the class name of the object managed by the repository.
     *
     * @return string
     */
    public function getClassName()
    {
        // TODO: Implement getClassName() method.
    }

    public function fetchLatest()
    {
        // TODO: Implement fetchLatest() method.
    }
}
