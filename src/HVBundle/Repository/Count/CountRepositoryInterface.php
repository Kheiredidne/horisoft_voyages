<?php
/**
 * Created by PhpStorm.
 * User: khayri
 * Date: 20/03/2016
 * Time: 02:25
 */

namespace HVBundle\Repository\Count;
use Doctrine\Common\Persistence\ObjectRepository;
use HVBundle\Entity\Count;


interface CountRepositoryInterface extends ObjectRepository
{

    public function addCount(Count $count);

}