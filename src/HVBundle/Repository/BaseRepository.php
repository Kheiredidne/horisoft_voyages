<?php
/**
 * Created by PhpStorm.
 * User: khayri
 * Date: 20/03/2016
 * Time: 22:58
 */

namespace HVBundle\Repository;



abstract class BaseRepository
{
protected function persistAndFlush($entity)
{
$this->em->persist($entity);

$this->em->flush();
}

}